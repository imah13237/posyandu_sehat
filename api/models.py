from django.db import models

class Ibu(models.Model):
    nama = models.CharField(max_length=100, verbose_name="Nama Ibu")
    usia = models.IntegerField(null=True, verbose_name="Usia")
    pekerjaan = models.CharField(max_length=100, verbose_name="Pekerjaan")
    status_pernikahan = models.CharField(max_length=100, verbose_name="Status Pernikahan")
    riwayat_kehamilan_persalinan = models.TextField(verbose_name="Riwayat Kehamilan Persalinan")
    catatan_kesehatan = models.TextField(verbose_name="Catatan Kesehatan")

    def __str__(self):
        return self.nama

class Balita(models.Model):
    nama = models.CharField(max_length=100, verbose_name="Nama Balita")
    tanggal_lahir_balita = models.DateField(verbose_name="Tanggal Lahir Balita")
    jenis_kelamin = models.CharField(max_length=20, verbose_name="Jenis Kelamin")
    berat_badan = models.DecimalField(max_digits=5, decimal_places=2, verbose_name="Berat Badan(kg)")
    imunisasi = models.TextField(verbose_name="Imunisasi")
    status_gizi = models.CharField(max_length=255, verbose_name="Status gizi")
    catatan_kesehatan = models.TextField(verbose_name="Catatan Kesehatan")
    ibu = models.ForeignKey(Ibu, related_name="Balita", on_delete=models.CASCADE, verbose_name="Ibu")
    

    def __str__(self):
        return self.nama

class Bidan(models.Model):
    nama = models.CharField(max_length=100, verbose_name="Nama Bidan")
    balita = models.ManyToManyField(Balita, verbose_name="Balita")

    def __str__(self):
        return self.nama

class Posyandu(models.Model):
    nama = models.CharField(max_length=100, verbose_name="Nama Posyandu")
    alamat = models.CharField(max_length=100, verbose_name="Alamat")
    jadwal = models.CharField(max_length=100, verbose_name="Jadwal")
    bidan = models.ManyToManyField(Bidan, verbose_name="Bidan")


    def __str__(self):
        return self.nama



