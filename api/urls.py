from django.urls import path
from django.contrib import admin
from . import views  

urlpatterns = [
    
    path("Balita/", views.BalitaListCreate.as_view(), name="Balita-views-create"),
    path("Balita/<int:pk>/", views.BalitaRetrieveUpdateDestroy.as_view(), name="update"),
    path("Ibu/", views.IbuListCreate.as_view(), name="Ibu-views-create"),
    path("Ibu/<int:pk>/", views.IbuRetrieveUpdateDestroy.as_view(), name="update"),
    path("Bidan/", views.BidanListCreate.as_view(), name="Bidan-views-create"),
    path("Bidan/<int:pk>/", views.BidanRetrieveUpdateDestroy.as_view(), name="update"),
    path("Posyandu/", views.PosyanduListCreate.as_view(), name="Posyandu-views-create"),
    path("Posyandu/<int:pk>/", views.PosyanduRetrieveUpdateDestroy.as_view(), name="update"),
    
]
