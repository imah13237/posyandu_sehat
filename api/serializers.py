from rest_framework import serializers
from .models import Balita, Ibu, Bidan, Posyandu

class BalitaSerializer(serializers.ModelSerializer):
   # Bidan = serializers.FrongKeyField(many=True)
    class Meta:
        model = Balita
        fields = ["id", "nama", "tanggal_lahir_balita", "jenis_kelamin", "berat_badan", "imunisasi", 
                "status_gizi", "catatan_kesehatan", "ibu"]

class IbuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ibu
        fields = ["id", "nama", "usia", "pekerjaan", "status_pernikahan", "riwayat_kehamilan_persalinan", 
                "catatan_kesehatan"]

class BidanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bidan
        fields = ["id", "nama", "balita"]

class PosyanduSerializer(serializers.ModelSerializer):
    class Meta:
        model = Posyandu
        fields = ["id",   "nama", "alamat", "jadwal", "bidan" ]